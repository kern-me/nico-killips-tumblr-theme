# Nico-Killips-Tumblr-Theme
Tumblr Theme modifications

This theme is a fork of "Ten Toes" theme from Tumblr's theme marketplace.

Theme: Ten Toes  
Help and FAQ: http://themes.stashfamily.com/help 
Download: https://www.tumblr.com/theme/40683
Documentation: http://themes.stashfamily.com/docs/ten-toes/
